---
home: true
heroImage: https://risingsunblog.gitee.io/shenling-logistics-template/static/img/logo.58dd185e.png
heroText: 神领物流TMS后台系统
# tagline: 这是副标题
actionText: 打开TMS →
actionLink: ./about/
features:
- title:  技术栈
  details: VUE2+VUEX+AXIOS
- title: 开发工具 
  details: vscode 
- title: node相关 
  details: node 版本 v14.19.1 npm 版本 v6.14.16

footer: MIT Licensed | Copyright © 2018-present phyger
---
# 神领物流 - 管理后台（PC端）
## 项目介绍:
​ 神领物流是一款专业的物流智能系统，覆盖城配、城际、快递等多种业务，包括订单分配、智能调度、线路规划、运费模板，快递配送等核心功能，实现了整个物流快递业务的智能管理。
<br/>
<br/>
​&nbsp;
​&nbsp;
​&nbsp;
​&nbsp;
​&nbsp;
本次项目做的是物流管理类项目，其中主要业务包括运费管理、车型管理、车辆管理、司机管理、排班管理、订单管理等内功能，主要针对的用户群体是物流公司内部的部门的管理人员，开发周期3天，项目团队共7人负责项目中拆分出来的6项功能模块，每个成员负责一项功能模块，剩余一人协助解决未完成模块中的较复杂功能。
## 管理后台：
该后台包括了从收到订单开始，通过仓库拣货发货，再通过分拨进行运输、末端配送，然后进行考核、结算等功能

